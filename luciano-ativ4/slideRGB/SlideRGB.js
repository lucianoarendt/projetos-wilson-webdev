const r = document.getElementById("R");
const g = document.getElementById("G");
const b = document.getElementById("B");
const caixa = document.getElementById("caixa");

function change_color(){
    const red = r.value;
    const green = g.value;
    const blue = b.value;

    caixa.style.backgroundColor = "rgb("+red+","+green+","+blue+")";
}

change_color();

r.addEventListener("input", change_color);
g.addEventListener("input", change_color);
b.addEventListener("input", change_color);

r.addEventListener("change", change_color);
g.addEventListener("change", change_color);
b.addEventListener("change", change_color);
