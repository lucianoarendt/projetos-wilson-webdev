class CalcIR{
    constructor(tabela){
        this.tabela = tabela;
    }

    calc(sal){
        for (let i = 0; i < this.tabela.length; i++) {
            const base = tabela[i];
            
            const imposto = base.calc(sal);
            if(imposto>=0){
                return imposto;
            }
        }
    }
}

class BaseIR{
    constructor(init, end, aliq, ded){
        this.init = init;
        this.end = end;
        this.aliq = aliq;
        this.ded = ded;
    }

    calc(sal){
        if((sal >= this.init && sal <= this.end) || this.end < 0){
            const imposto = this.aliq*sal - this.ded;
            return (imposto<0)?0:imposto;
        }else return -1;
    }
}

const tabela = [
    //          start    end     aliq   deduc
    new BaseIR(      0, 1903.98,     0,      0),
    new BaseIR(1903.99, 2826.65, 0.075,  142.8),
    new BaseIR(2826.66, 3751.05,  0.15,  354.8),
    new BaseIR(3751.06, 4664.68, 0.225, 636.13),
    new BaseIR(4664.69,      -1, 0.275, 869.36),
];
const calculadora = new CalcIR(tabela);

const sal = document.getElementById("sal");
const display = document.getElementById("display");

sal.addEventListener("keypress", e =>{
    if(e.key === "Enter"){
        imprimeCalcIR();
    }
});
document.getElementById("calc").addEventListener("click", imprimeCalcIR);

function imprimeCalcIR(){
    const imposto = calculadora.calc(sal.value);
    display.innerHTML = `O imposto a pagar eh ${Math.round(imposto*100)/100}`;
}