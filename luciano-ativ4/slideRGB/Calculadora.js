const dsp = document.getElementById("scr");
let clr = false;

document.getElementsByName("numop").forEach(el => {
    el.addEventListener("click", ()=>{
        if(clr){
            dsp.innerHTML = "";
            clr = false;
        }

        dsp.innerHTML += el.innerHTML;
    })
});

document.getElementById("equals").addEventListener("click", ()=>{
    const res = math.eval(dsp.innerHTML); 
    dsp.innerHTML += " = " + res;

    clr=true;
});

document.getElementById("clr").addEventListener("click", ()=>{
    dsp.innerHTML = "";
})