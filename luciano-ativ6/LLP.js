const equipes = [];
const alunos = [
    "Jeffinho", "Lucas", "Miguel",
    "Arthur", "Gael", "Théo", "Heitor",
    "Ravi",  "Davi", "Bernardo", "Luciano", "Clayton",
    "Luana", "Leticia", "Larissa", "Lucia", "Lorraine",
    "Lidia", "Laura"
];

const equipes_dom = document.getElementById("equipes");
const nomeEquipe_dom = document.getElementById("nome");

function criarEquipe(){
    const n_equipe = {nome:nomeEquipe_dom.value, alunos:[]};
    equipes.push(n_equipe);
    criarEquipeDom(n_equipe, equipes.length-1);
}

function criarEquipeDom(e, ie){
    equipes_dom.innerHTML += `
    <div id="equipe-${ie}">
        <h2>${e.nome}</h2>
        <span>Adicionar Aluno:</span>
        <select id="sel-al-${ie}">
            ${alunos.map((a,ia)=>`<option val=${ia}>${a}</option>`)}
        </select>
        <button onClick="addAlunoEquipe(${ie})">Adicionar Aluno</button>

        <ul id="list-al-${ie}">

        </ul>
        <button onClick="removeEquipe(${ie})">Remover Equipe</button>
        <hr/>
    </div>
    `;
}

function addAlunoEquipe(ie){
    console.log(ie);
    const sel_al_dom = document.getElementById(`sel-al-${ie}`);
    const nome = sel_al_dom.value;

    equipes[ie].alunos.push(nome);

    const list_al_dom = document.getElementById(`list-al-${ie}`);
    list_al_dom.innerHTML += `
        <li>${nome}</li>
    `;
}

function removeEquipe(ie){
    equipes.splice(ie, 1);
    document.getElementById(`equipe-${ie}`).remove();

    console.log(equipes);
}