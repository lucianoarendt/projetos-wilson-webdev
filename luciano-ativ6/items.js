class Item{
    constructor(nome, qtd){
        this.nome = nome;
        this.qtd_dep = qtd;
        this.qtd_aula = 0;
    }

    add_to_aula(i=1) {
        if(this.qtd_dep - i < 0) return false;

        this.qtd_dep -= i;
        this.qtd_aula += i;
        return true;
    }

    rem_from_aula(i=1){
        if(this.qtd_aula - i < 0) return false;

        this.qtd_aula -= i;
        this.qtd_dep += i;
        return true;
    }

    rem_all_aula(){
        this.rem_from_aula(this.qtd_aula);
    }

    add_all_aula(){
        this.add_to_aula(this.qtd_dep);
    }

    qtd_total(){
        return this.qtd_aula + this.qtd_dep;
    }
}


function create_ithaula(item, i){
    return `
        <div class="items" id="it-a-${i}">
            <div class="it-col-bt">
                <button class="bt bt-bg it-col" onClick="remHAula(${i})"><<</button>
            </div>
            <span class="it-col it-qtd" id="it-a-qtd-${i}">${item.qtd_aula} / ${item.qtd_total()}</span>
            <span class="it-col it-name">${item.nome}</span>
        </div>
    `;
}

function create_ithdep(item, i){
    return `
        <div class="items" id="it-d-${i}">
            <span class="it-col">${item.nome}</span>
            <span class="it-col it-qtd" id="it-d-qtd-${i}">${item.qtd_dep} / ${item.qtd_total()}</span>
            <div class="it-col-bt">
                <button class="bt bt-bg" onClick="addHAula(${i})">>></button>
            </div>          
        </div>
    `;
}

function update_totais(){
    const tt_a = items.reduce((prev, e)=> prev + e.qtd_aula, 0);
    const tt_d = items.reduce((prev, e)=> prev + e.qtd_dep, 0);

    document.getElementById("total_a").innerHTML = `Total : ${tt_a}`;
    document.getElementById("total_d").innerHTML = `Total : ${tt_d}`;
}

function addHAula(i){
    const item = items[i];
    if(!item.add_to_aula()) return;

    if(item.qtd_aula == 1){
        h_aula.innerHTML += create_ithaula(item, i);
    }
    else{
        const it = document.getElementById(`it-a-qtd-${i}`);
        it.innerHTML = `${item.qtd_aula} / ${item.qtd_total()}`;
    }

    if(item.qtd_dep == 0){
        const it = document.getElementById(`it-d-${i}`);
        it.remove();
    }else{
        const it = document.getElementById(`it-d-qtd-${i}`);
        it.innerHTML = `${item.qtd_dep} / ${item.qtd_total()}`;
    }

    update_totais();
}

function remHAula(i){
    const item = items[i];
    if(!item.rem_from_aula()) return;

    if(item.qtd_dep == 1){
        h_dep.innerHTML += create_ithdep(item, i);
    }
    else{
        const it = document.getElementById(`it-d-qtd-${i}`);
        it.innerHTML = `${item.qtd_dep} / ${item.qtd_total()}`;
    }

    if(item.qtd_aula == 0){
        const it = document.getElementById(`it-a-${i}`);
        it.remove();
    }else{
        const it = document.getElementById(`it-a-qtd-${i}`);
        it.innerHTML = `${item.qtd_aula} / ${item.qtd_total()}`;
    }

    update_totais();
}

const h_dep = document.getElementById("h_dep");
const h_aula = document.getElementById("h_aula");

const items = [
    new Item("Resistor", 50),
    new Item("Multimetro", 5),
    new Item("Protoboard", 10),
    new Item("LED", 20),
    new Item("Kit Jumper", 10)
];

items.forEach((e, i) => {
    h_dep.innerHTML += create_ithdep(e, i);
});

update_totais();