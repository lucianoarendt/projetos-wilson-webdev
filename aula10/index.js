const express = require('express');
const app = express();
const port = 3000;
const mysql      = require('mysql2');
function execSQLQuery(sqlQry, res){
   const connection = mysql.createConnection({
       host     : 'localhost',
       port     : 3306,
       user     : 'root',
       password : '123456',
       database : 'pessoal'
   }); 
    connection.query(sqlQry, (error, results, fields) => {
       if(error) 
         res.json(error);
       else
         res.json(results);
       connection.end();
       console.log('Ok. Ação executada!');
   });
}
// Rotas
app.get('/', (req, res) => {
   let qry = 'select * from usuarios';
   execSQLQuery(qry, res);
});
app.delete('/apaga/:id', (req, res) => {
   let qry = 'DELETE FROM usuarios WHERE ID=' + parseInt(req.params.id);
   console.log(qry);
   execSQLQuery(qry, res);

});
app.put('/insere/:id/:nome/:email', (req, res) => {
  let qry = 'INSERT INTO usuarios values (' + parseInt(req.params.id)+', "'+req.params.nome+'", "'+ req.params.email+'")';
  console.log(qry);
  execSQLQuery(qry, res);

});

// Servidor
app.listen(port);
console.log('Servidor iniciado na porta ' + port);
