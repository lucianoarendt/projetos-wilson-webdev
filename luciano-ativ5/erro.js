const erro = document.getElementById("erro");
const nome = document.getElementById("nome");
const apelido = document.getElementById("apelido");
const nasc = document.getElementById("nasc");
const tel = document.getElementById("telefone");
const email = document.getElementById("email");
const ano = document.getElementById("ano");
const dep = document.getElementById("dep");

function enviarDep(){
    erro.style.color = "rgb(237, 59, 59)";
    erro.innerHTML = "";

    if(nome.value == ""){
        erro.innerHTML = "*Por favor insira seu nome*";
        return;
    }

    if(apelido.value == ""){
        erro.innerHTML = "*Por favor insira seu apelido*";
        return;
    }

    if(email.value == ""){
        erro.innerHTML = "*Por favor insira seu email*";
        return;
    }

    if(dep.value == ""){
        erro.innerHTML = "*Por favor insira seu depoimento*";
        return;
    }

    if(ano.value == ""){
        erro.innerHTML = "*Por favor insira seu ano de ingresso*";
        return;
    }

    if(tel.value == ""){
        erro.innerHTML = "*Por favor insira seu telefone*";
        return;
    }

    if(nasc.value == ""){
        erro.innerHTML = "*Por favor insira sua data de nascimento*";
        return;
    }

    const aluno = {
        nome: nome.value,
        apelido: apelido.value,
        email: email.value,
        ano: ano.value,
        telefone: tel.value,
        nasc: nasc.value,
        dep: dep.value
    }

    const json = JSON.stringify(aluno);
    console.log(json);

    erro.innerHTML = "*Obrigado, "+ aluno.nome +", por seu depoimento*";
    erro.style.color = "green";
}