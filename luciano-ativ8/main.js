const express = require('express');
const app = express();

const registros = [];

app.get('/registro', (req, res)=>{
    res.send(registros);
});

app.get('/registro/:id', (req, res)=>{
    if(registros.length <= req.params.id){
        res.status(404).send("Not found");
        return;
    }

    res.send(registros[req.params.id]);
});

app.post('/registro/add', (req, res)=>{
    if(!req.body.nome){
        res.status(400).send("Nome nao pode ser vazio");
        return;
    }

    registros.push(req.body.nome);
});

app.listen(9000, ()=>{
    console.log("Servidor inciado na porta 9000")
});