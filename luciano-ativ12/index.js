const express = require("express");
const path    = require("path");
const sequelize = require('./utils/database');
const app = express();
const PORT = 3000.

sequelize.sync();

app.set('views', './views');
app.set('view engine', 'ejs');
app.set(express.static('public'));

// Rotas
const homeRoute  = require("./routes/homeRoutes");
const userRoute  = require("./routes/userRoutes");
const cursoRoute = require("./routes/cursoRoutes");
const disciplinaRoute = require("./routes/disciplinaRoutes");

app.use(homeRoute);
app.use(userRoute);
app.use(cursoRoute);
app.use(disciplinaRoute);

app.listen(PORT, () => {
   console.log(`Servidor on port ${PORT}`);
});
