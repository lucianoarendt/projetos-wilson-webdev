const express= require("express");
const disciplinaRouter = express.Router();
const disciplinaCtrl = require("../controllers/disciplinaController");

disciplinaRouter.get('/disciplina', disciplinaCtrl.GetAll);
disciplinaRouter.get('/disciplina/:id', disciplinaCtrl.GetDisciplina);
disciplinaRouter.delete('/disciplina/:id', disciplinaCtrl.DeleteDisciplina);

module.exports = disciplinaRouter;