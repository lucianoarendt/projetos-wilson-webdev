const express= require("express");
const homerouter = express.Router();
homerouter.get('/', (req, res) => {
   res.render('home',  {
      title: 'Home Page',
      nome:  'João Silva',
   })
});
module.exports = homerouter;