const express= require("express");
const userrouter = express.Router();
const userCtrl = require("../controllers/userController");

userrouter.get('/userall', userCtrl.GetAllUsers);
userrouter.get('/user/:nome', userCtrl.GetUser);
userrouter.post('/user/:nome/:email/:curso', userCtrl.CreateUser);
userrouter.delete('/user/:id', userCtrl.DeleteUser);

module.exports = userrouter;