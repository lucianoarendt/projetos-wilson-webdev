const express= require("express");
const cursorouter = express.Router();
const cursoCtrl = require("../controllers/cursoController");

cursorouter.get('/cursoall', cursoCtrl.GetAllCursos);
cursorouter.get('/curso/:id', cursoCtrl.GetCurso);
cursorouter.post('/curso/:nome/:sigla/:inicio', cursoCtrl.CreateCurso);
cursorouter.delete('/curso/:id', cursoCtrl.DeleteCurso);

module.exports = cursorouter;