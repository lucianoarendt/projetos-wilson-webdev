const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Curso = sequelize.define('cursos', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  nome: Sequelize.STRING,
  sigla: Sequelize.STRING,
  inicio: Sequelize.INTEGER
});

module.exports = Curso;
