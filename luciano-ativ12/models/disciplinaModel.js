const Sequelize = require('sequelize');
const sequelize = require('../utils/database');
const Disciplina = sequelize.define('disciplinas', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  descricao: Sequelize.STRING,
  creditos: Sequelize.INTEGER,
  curso: Sequelize.STRING
});

module.exports = Disciplina;