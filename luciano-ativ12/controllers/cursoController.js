const Curso = require('../models/cursoModel');

const GetAllCursos = async (req, res) => {
    const cursoList = await Curso.findAll();
    // Faça algo aqui
    console.log(cursoList);
    return res.json(cursoList);
}

const GetCurso = async (req, res) => {
   const cursoId = req.params.id;
   const curso = await Curso.findByPk(cursoId)
      .then( curso => {
         if (curso === null) {
            console.error(`Atenção: Curso ${cursoId} não encontrado!`)    
         } else {
            console.error(`Curso ${cursoId} encontrado!`)    
            console.log(curso.nome)
         }
         return res.json(curso);
      })
      .catch( error => {
        console.error(error)
      });
}

const DeleteCurso = async (req, res) => {
    const cursoId = req.params.id;
    const curso = await Curso.DeleteCurso(cursoId);
    // Faça algo aqui
    console.log(curso);
    return res.json(curso);
}

const CreateCurso = async (req, res) => {
   const nome = req.params.nome;
   const sigla = req.params.sigla;
   const inicio = req.params.inicio;

   const ret = await Curso.create({nome: nome, sigla:sigla, inicio:inicio});

   return res.json(ret);
}

module.exports = {
    GetAllCursos,
    GetCurso,
    DeleteCurso,
    CreateCurso,
};