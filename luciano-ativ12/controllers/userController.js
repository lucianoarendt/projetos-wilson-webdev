const User       = require('../models/userModel');

const GetAllUsers = async (req, res) => {
    const userList = await User.findAll();
    const usuario = userList;
    return res.render('userView',  {usuario});
}

const GetUser = async (req, res) => {
   const nome = req.params.nome;
   const user = await User.findAll({where:{nome: nome}})
      .then( user => {
         if (user === null) {
            console.error(`Atenção: Usuário ${nome} não encontrado!`)    
         } else {
            console.error(`Usuário ${nome} encontrado!`)    
            console.log(user.nome)
        }
        return res.json(user);
      })
      .catch( error => {
        console.error(error)
       });
}

const DeleteUser = async (req, res) => {
    const userId = req.params.id;
    const user = await User.DeleteUser(userId);
    // Faça algo aqui
    console.log(user);
    return res.json(user);
}

const CreateUser = async (req, res) => {
    const nome = req.params.nome;
    const email = req.params.email;
    const curso = req.params.curso;

    const ret = await User.create({nome: nome, email:email, curso:curso});

    return res.json(ret);
}

module.exports = {
    GetAllUsers,
    GetUser,
    DeleteUser,
    CreateUser,
};