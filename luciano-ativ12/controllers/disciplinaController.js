const Disciplina = require('../models/disciplinaModel');

const GetAll = async (req, res) => {
    const discList = await Disciplina.findAll();
    console.log(discList);
    return res.render('disciplinasView',  {discList});
}

const GetDisciplina = async (req, res) => {
   const id = req.params.id;
   const curso = await Disciplina.findByPk(id)
      .then( disciplina => {
         if (disciplina === null) {
            console.error(`Atenção: Curso ${id} não encontrado!`);   
         } else {
            console.error(`Curso ${id} encontrado!`);
            console.log(disciplina.descricao);
         }
         return res.json(curso);
      })
      .catch( error => {
        console.error(error);
      });
}

const DeleteDisciplina = async (req, res) => {
    const id = req.params.id;
    const curso = await Disciplina.destroy({where:{id: id}});
    
    console.log(curso);
    return res.json(curso);
}

module.exports = {
    GetAll,
    GetDisciplina,
    DeleteDisciplina,
};