const erro = document.getElementById("erro");
const nome = document.getElementById("nome");
const email = document.getElementById("email");
const ano = document.getElementById("ano");
const dep = document.getElementById("dep");

function enviarDep(){
    erro.style.color = "rgb(237, 59, 59)";
    erro.innerHTML = "";

    if(nome.value == ""){
        erro.innerHTML = "*Por favor insira seu nome*";
        return;
    }

    if(email.value == ""){
        erro.innerHTML = "*Por favor insira seu email*";
        return;
    }

    if(dep.value == ""){
        erro.innerHTML = "*Por favor insira seu depoimento*";
        return;
    }

    if(ano.value == ""){
        erro.innerHTML = "*Por favor insira seu ano de ingresso*";
        return;
    }

    erro.innerHTML = "*Obrigado, "+ nome.value +", por seu depoimento*";
    erro.style.color = "green";
}