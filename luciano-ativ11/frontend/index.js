function buscarUsuarios(){
    const ul = document.getElementById("list_usu");
    ul.innerHTML = "";

    fetch('http://localhost:3000/')
    .then(resp => {
        if(resp.ok){
            return resp.json();
        }else{
            ul.innerHTML = resp.statusText;
        }
    })
    .then(json =>{
        json.forEach(e => {
            ul.innerHTML += `<li>ID:${e.id}, Nome:${e.nome}, Email:${e.email}, Curso:${e.curso}</li>`;
        });
    });
}

function deletarClick(){
    const id_del = document.getElementById("id_del");
    const aviso_del = document.getElementById("aviso_del");
    const id = id_del.value;

    fetch(`http:/localhost:3000/apaga/${id}`,{
        method: "DELETE"
    }).then(resp =>{
        if(resp.ok){
            aviso_del.innerHTML = "Apagado com sucesso!";
        }else{
            aviso_del.innerHTML = resp.statusText;
        }
    });
}

function adicionarClick(){
    const aviso_add = document.getElementById("aviso_add");

    const curso = document.getElementById("curso_add").value;
    const nome = document.getElementById("nome_add").value;
    const email = document.getElementById("email_add").value;

    fetch(`http:/localhost:3000/insere/${nome}/${email}/${curso}`,{
        method: "POST"
    }).then(resp =>{
        if(resp.ok){
            aviso_add.innerHTML = "Inserido com sucesso!";
        }else{
            aviso_add.innerHTML = resp.statusText;
        }
    })
}