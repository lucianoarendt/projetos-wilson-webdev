const express = require('express');
const cors = require('cors');
const app = express();

const Usuario = require("./models/usuario");

Usuario.sync({ force: true });

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});

app.get('/', async (req, res)=>{
    let result = await Usuario.findAll();
    res.json(result);
});

app.delete('/apaga/:id', async (req, res) => {
    let result = await Usuario.destroy({
        where:{id: req.params.id}
    });
    res.json(result);
});

app.post('/insere/:nome/:email/:curso', async (req, res) => {
    let result = await Usuario.create({
        nome: req.params.nome,
        email: req.params.email,
        curso: req.params.curso
    });
    res.json(result);
});

app.listen(3000);
